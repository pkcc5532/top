@extends('layouts.home_layout', ['static_data', $static_data])
<?php $max_price = get_setting('price_range_max', 'property') ?>
@section('title')
    <title>{{$static_data['strings']['search_results']}}</title>
    <meta charset="UTF-8">
    <meta name="title" content="{{ $static_data['site_settings']['site_name'] }}">
    <meta name="description" content="{{ $static_data['site_settings']['site_description'] }}">
    <meta name="keywords" content="{{ $static_data['site_settings']['site_keywords'] }}">
    <meta name="author" content="{{ $static_data['site_settings']['site_name'] }}">
    <meta property="og:title" content="{{ $static_data['site_settings']['site_name'] }}" />
    <meta property="og:image" content="{{URL::asset('/assets/images/home/').'/'.$static_data['design_settings']['slider_background']}}" />
@endsection
@section('head')
    <link rel="stylesheet" href="{{ URL::asset('assets/css/plugins/nouislider.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/plugins/jquery-ui.min.css') }}">
@endsection
@section('bg')
    {{URL::asset('/assets/images/home/').'/'.$static_data['design_settings']['slider_background']}}
@endsection
@section('content')
    <div class="row header-tabs">
        <div class="col-sm-12">
            <ul class="nav nav-tabs" id="header-tabs">
                @if(isset($properties) && count($properties))<li class="nav-item"><a class="nav-link active" href="#accordion-properties" data-toggle="tab" aria-expanded="true"><i class="fa fa-building-o"></i><span>{{ $static_data['strings']['properties'] }}</span></a></li>@endif
                @if(isset($services) && count($services))<li class="nav-item"><a class="nav-link @if(!count($properties)) active @endif" href="#accordion-services" data-toggle="tab" aria-expanded="false"><i class="fa fa-cutlery"></i><span>{{ $static_data['strings']['services'] }}</span></a></li>@endif
            </ul>
        </div>
    </div>
    <div class="row marginalized">
        <div class="col-sm-12">
            <h1 class="section-title-dark">{{$static_data['strings']['search_results']}}</h1>
            <div class="tab-content">
                <div class="tab-pane active" id="accordion-properties" role="tabpanel">
                    <div class="row">
                        @if(count($properties))
                            <div class="col-sm-12">
                                <h3 class="section-type text-uppercase">{{ $static_data['strings']['properties'] }}</h3>
                            </div>
                            <div class="col-sm-12 filter-box">
                                {!! Form::open(['method' => 'post', 'url' => route('search')]) !!}
                                    <div class="form-group not-after">
                                        <div class="input-group">
                                            <span class="fa fa-font input-group-addon"></span>
                                            <input type="text" value="" name="keyword" class="form-control slider-field" placeholder="{{$static_data['strings']['keywords']}} ...">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="fa fa-map-marker input-group-addon"></span>
                                            <input type="text" readonly name="location_id_value" class="form-control filter-field" placeholder="{{$static_data['strings']['choose_your_location']}}">
                                        </div>
                                        <input type="hidden" name="location_id" value="0" class="form-control filter-hidden hidden" placeholder="{{$static_data['strings']['choose_your_location']}}">
                                        <ul class="dropdown-filter-menu">
                                            <li data-id="" data-name="{{ $static_data['strings']['all'] }}">
                                                <a href="#" class="location_id_picker">
                                                    <span>{{ $static_data['strings']['all'] }}</span>
                                                </a>
                                            </li>
                                            @foreach($static_data['locations'] as $location)
                                                <li data-id="{{ $location->id }}" data-name="{{ $location->contentload->location }}">
                                                    <a href="#" class="location_id_picker">
                                                        <span>{{ $location->contentload->location }}</span>
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="fa fa-map-marker input-group-addon"></span>
                                            <input type="text" readonly name="category_id_value" class="form-control filter-field" placeholder="{{$static_data['strings']['choose_your_category']}}">
                                        </div>
                                        <input type="hidden" name="category_id" value="0" class="form-control filter-hidden hidden" placeholder="{{$static_data['strings']['choose_your_category']}}">
                                        <ul class="dropdown-filter-menu">
                                            <li data-id="" data-name="{{ $static_data['strings']['all'] }}">
                                                <a href="#" class="category_id_picker">
                                                    <span>{{ $static_data['strings']['all'] }}</span>
                                                </a>
                                            </li>
                                            @foreach($static_data['categories'] as $category)
                                                <li data-id="{{ $category->id }}" data-name="{{ $category->contentload->name }}">
                                                    <a href="#" class="category_id_picker">
                                                        <span>{{ $category->contentload->name }}</span>
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <button type="submit" class="primary-button"><i class="fa fa-search"></i> {{$static_data['strings']['search']}}</button>
                                {!! Form::close() !!}
                            </div>
                            <!-- @if(isset($featured_properties))
                                <div class="col-sm-12 featured-grid-properties items-grid">
                                    @foreach($featured_properties as $property)
                                        <div class="item box-shadow">
                                            <div id="carousel-_{{$property->id}}" class="main-image bg-overlay carousel slide" data-ride="carousel" data-interval="false">
                                                <div class="featured-sign">
                                                    {{ $static_data['strings']['featured'] }}
                                                </div>
                                                <div class="price">
                                                    <span class="currency"></span> {{ currency((int)$property->price_per_night, $static_data['site_settings']['currency_code'], Session::get('currency')) }} <span class="currency"> {{ $static_data['strings']['per_night'] }}</span>
                                                </div>
                                                @if(count($property->images))
                                                    <div class="carousel-inner" role="listbox">
                                                        <?php $c = 0; ?>
                                                        @foreach($property->images as $image)
                                                            <div class="carousel-item @if(!$c) active <?php $c++; ?> @endif">
                                                                <img class="responsive-img" src="{{ URL::asset('images/data').'/'.$image->image }}"/>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <a class="carousel-control-prev" href="#carousel-_{{$property->id}}" role="button" data-slide="prev">
                                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                        <span class="sr-only">{{$static_data['strings']['previous']}}</span>
                                                    </a>
                                                    <a class="carousel-control-next" href="#carousel-_{{$property->id}}" role="button" data-slide="next">
                                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                        <span class="sr-only">{{$static_data['strings']['next']}}</span>
                                                    </a>
                                                @else
                                                    <div class="carousel-inner" role="listbox">
                                                        <div class="carousel-item active">
                                                            <img class="responsive-img" src="{{ URL::asset('images/').'/no_image.jpg' }}"/>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="data">
                                                <a href="{{url('/property').'/'.$property->alias}}"><h3 class="item-title primary-color">{{ $property->contentload->name }}</h3></a>
                                                <div class="item-category">{{$property->location['address'].', '.$property->location['city'] .' - '. $property->location['country']}}</div>
                                                <div class="item-category">{{ $static_data['strings']['category'] .': '. $property->category->contentload->name .' | ' }}
                                                    {{ $static_data['strings']['location'] .': '. $property->prop_location->contentload->location }}</div>                        <div class="item-category">{{ $static_data['strings']['size'] .': '. $property->property_info['size'] . ' '. $static_data['site_settings']['measurement_unit']. ' | '}}
                                                    {{ $static_data['strings']['rooms'] .': '. $property->rooms .' | '}}
                                                    {{ $static_data['strings']['guests'] .': '. $property->guest_number}}</div>
                                                <div class="small-text">{{ $static_data['strings']['posted_by'] .': '. $property->user->username }}</div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif -->
                            <div id="filtered-properties" class="row">
                                @foreach($properties as $property)
                                    <div class="col-md-4 col-sm-6 items-grid">
                                        <div class="item box-shadow">
                                            <div id="carousel-{{$property->id}}" class="main-image bg-overlay carousel slide" data-ride="carousel" data-interval="false">
                                                @if($property->featured)
                                                    <div class="featured-sign">
                                                        {{ $static_data['strings']['featured'] }}
                                                    </div>
                                                @endif
                                                <div class="price">
                                                   <span class="currency"></span> {{ currency((int)$property->price_per_night, $static_data['site_settings']['currency_code'], Session::get('currency')) }} <span class="currency"> {{ $static_data['strings']['per_night'] }}</span>
                                                </div>
                                                @if(count($property->images))
                                                    <div class="carousel-inner" role="listbox">
                                                        <?php $c = 0; ?>
                                                        @foreach($property->images as $image)
                                                            <div class="carousel-item @if(!$c) active <?php $c++; ?> @endif">
                                                                <img class="responsive-img" src="{{ URL::asset('images/data').'/'.$image->image }}"/>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <a class="carousel-control-prev" href="#carousel-{{$property->id}}" role="button" data-slide="prev">
                                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                        <span class="sr-only">{{$static_data['strings']['previous']}}</span>
                                                    </a>
                                                    <a class="carousel-control-next" href="#carousel-{{$property->id}}" role="button" data-slide="next">
                                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                        <span class="sr-only">{{$static_data['strings']['next']}}</span>
                                                    </a>
                                                @else
                                                    <div class="carousel-inner" role="listbox">
                                                        <div class="carousel-item active">
                                                            <img class="responsive-img" src="{{ URL::asset('images/').'/no_image.jpg' }}"/>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="data">
                                                <a href="{{url('/property').'/'.$property->alias}}"><h3 class="item-title primary-color">{{ $property->contentload->name }}</h3></a>
                                                <div class="item-category">{{$property->location['address'].', '.$property->location['city'] .' - '. $property->location['country']}}</div>
                                                <div class="item-category">{{ $static_data['strings']['category'] .': '. $property->category->contentload->name .' | ' }}
                                                    {{ $static_data['strings']['location'] .': '. $property->prop_location->contentload->location }}</div>
                                                <div class="item-category">{{ $static_data['strings']['size'] .': '. $property->property_info['size'] . ' '. $static_data['site_settings']['measurement_unit']. ' | '}}
                                                    {{ $static_data['strings']['rooms'] .': '. $property->rooms .' | '}}
                                                    {{ $static_data['strings']['guests'] .': '. $property->guest_number}}</div>
                                                @if($property->user)<div class="small-text">{{ $static_data['strings']['posted_by'] .': '. $property->user->username }}</div>@endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            @if(!count($properties) && !count($services))<div class="col-sm-12 text-centered"><strong class="center-align">{{$static_data['strings']['no_results']}}</strong></div>@endif
                        @endif
                    </div>
                </div>
                @if(get_setting('services_allowed', 'service'))
                    <div class="tab-pane @if(!count($properties)) active @endif" id="accordion-services" role="tabpanel">
                        <div class="row">
                            @if(isset($services) && count($services))
                                <div class="col-sm-12 mbot10"><h3 class="section-type text-uppercase">{{ $static_data['strings']['services'] }}</h3></div>
                                <div class="col-sm-12 filter-box">
                                    {!! Form::open(['method' => 'post', 'url' => route('search')]) !!}
                                    <div class="form-group not-after">
                                        <div class="input-group">
                                            <span class="fa fa-font input-group-addon"></span>
                                            <input type="text" value="" name="keyword" class="form-control slider-field" placeholder="{{$static_data['strings']['keywords']}} ...">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="fa fa-map-marker input-group-addon"></span>
                                            <input type="text" readonly name="location_id_value" class="form-control filter-field" placeholder="{{$static_data['strings']['choose_your_location']}}">
                                        </div>
                                        <input type="hidden" name="location_id" value="0" class="form-control filter-hidden hidden" placeholder="{{$static_data['strings']['choose_your_location']}}">
                                        <ul class="dropdown-filter-menu">
                                            <li data-id="" data-name="{{ $static_data['strings']['all'] }}">
                                                <a href="#" class="location_id_picker">
                                                    <span>{{ $static_data['strings']['all'] }}</span>
                                                </a>
                                            </li>
                                            @foreach($static_data['locations'] as $location)
                                                <li data-id="{{ $location->id }}" data-name="{{ $location->contentload->location }}">
                                                    <a href="#" class="location_id_picker">
                                                        <span>{{ $location->contentload->location }}</span>
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="fa fa-map-marker input-group-addon"></span>
                                            <input type="text" readonly name="category_id_value" class="form-control filter-field" placeholder="{{$static_data['strings']['choose_your_category']}}">
                                        </div>
                                        <input type="hidden" name="category_id" value="0" class="form-control filter-hidden hidden" placeholder="{{$static_data['strings']['choose_your_category']}}">
                                        <ul class="dropdown-filter-menu">
                                            <li data-id="" data-name="{{ $static_data['strings']['all'] }}">
                                                <a href="#" class="category_id_picker">
                                                    <span>{{ $static_data['strings']['all'] }}</span>
                                                </a>
                                            </li>
                                            @foreach($static_data['categories'] as $category)
                                                <li data-id="{{ $category->id }}" data-name="{{ $category->contentload->name }}">
                                                    <a href="#" class="category_id_picker">
                                                        <span>{{ $category->contentload->name }}</span>
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <button type="submit" class="primary-button"><i class="fa fa-search"></i> {{$static_data['strings']['search']}}</button>
                                    {!! Form::close() !!}
                                </div>
                                <!-- @if(isset($featured_services))
                                    <div class="col-sm-12 featured-grid-services items-grid">
                                        @foreach($featured_services as $service)
                                            <div class="item box-shadow">
                                                <div id="carousel_-{{$service->id}}" class="main-image bg-overlay carousel slide" data-ride="carousel" data-interval="false">
                                                    <div class="featured-sign">
                                                        {{ $static_data['strings']['featured'] }}
                                                    </div>
                                                    @if(count($service->images))
                                                        <div class="carousel-inner" role="listbox">
                                                            <?php $c = 0; ?>
                                                            @foreach($service->images as $image)
                                                                <div class="carousel-item @if(!$c) active <?php $c++; ?> @endif">
                                                                    <img class="responsive-img" src="{{ URL::asset('images/data').'/'.$image->image }}"/>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                        <a class="carousel-control-prev" href="#carousel_-{{$service->id}}" role="button" data-slide="prev">
                                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                            <span class="sr-only">{{$static_data['strings']['previous']}}</span>
                                                        </a>
                                                        <a class="carousel-control-next" href="#carousel_-{{$service->id}}" role="button" data-slide="next">
                                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                            <span class="sr-only">{{$static_data['strings']['next']}}</span>
                                                        </a>
                                                    @else
                                                        <div class="carousel-inner" role="listbox">
                                                            <div class="carousel-item active">
                                                                <img class="responsive-img" src="{{ URL::asset('images/').'/no_image.jpg' }}"/>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="data">
                                                    <a href="{{url('/service').'/'.$service->alias}}"><h3 class="item-title primary-color">{{ $service->contentload->name }}</h3></a>
                                                    <div class="item-category">{{$service->location['address'].', '.$service->location['city'] .' - '. $service->location['country']}}</div>
                                                    <div class="item-category">{{$service->category->contentload->name.' - '.$service->ser_location->contentload->location}}</div>
                                                    <div class="small-text">{{ $static_data['strings']['posted_by'] .': '. $service->user->username }}</div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif -->
                        </div>
                        <div class="row" id="filtered-services">
                            @foreach($services as $service)
                                <div class="col-md-4 col-sm-6 items-grid">
                                    <div class="item box-shadow">
                                        <div id="carousel--{{$service->id}}" class="main-image bg-overlay carousel slide" data-ride="carousel" data-interval="false">
                                            @if($service->featured)
                                                <div class="featured-sign">
                                                    {{ $static_data['strings']['featured'] }}
                                                </div>
                                            @endif
                                            @if(count($service->images))
                                                <div class="carousel-inner" role="listbox">
                                                    <?php $c = 0; ?>
                                                    @foreach($service->images as $image)
                                                        <div class="carousel-item @if(!$c) active <?php $c++; ?> @endif">
                                                            <img class="responsive-img" src="{{ URL::asset('images/data').'/'.$image->image }}"/>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <a class="carousel-control-prev" href="#carousel--{{$service->id}}" role="button" data-slide="prev">
                                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                    <span class="sr-only">{{$static_data['strings']['previous']}}</span>
                                                </a>
                                                <a class="carousel-control-next" href="#carousel--{{$service->id}}" role="button" data-slide="next">
                                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                    <span class="sr-only">{{$static_data['strings']['next']}}</span>
                                                </a>
                                            @else
                                                <div class="carousel-inner" role="listbox">
                                                    <div class="carousel-item active">
                                                        <img class="responsive-img" src="{{ URL::asset('images/').'/no_image.jpg' }}"/>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="data">
                                            <a href="{{url('/service').'/'.$service->alias}}"><h3 class="item-title primary-color">{{ $service->contentload->name }}</h3></a>
                                            <div class="item-category">{{$service->location['address'].', '.$service->location['city'] .' - '. $service->location['country']}}</div>
                                            <div class="item-category">{{$service->category->contentload->name.' - '.$service->ser_location->contentload->location}}</div>
                                            @if($service->user)<div class="small-text">{{ $static_data['strings']['posted_by'] .': '. $service->user->username }}</div>@endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('footer')
@endsection