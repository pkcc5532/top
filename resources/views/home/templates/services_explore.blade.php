@if($services->count())
    @foreach($services as $service)
        <div class="col-md-6 col-sm-6 items-grid">
            <div class="item box-shadow">
                <div id="carousel--{{$service->id}}" class="main-image bg-overlay carousel slide" data-ride="carousel" data-interval="false">
                    @if(count($service->images))
                        <div class="carousel-inner" role="listbox">
                            <?php $c = 0; ?>
                            @foreach($service->images as $image)
                                <div class="carousel-item @if(!$c) active <?php $c++; ?> @endif">
                                    <img class="responsive-img" src="{{ URL::asset('images/data').'/'.$image->image }}"/>
                                </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carousel--{{$service->id}}" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">{{$static_data['strings']['previous']}}</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel--{{$service->id}}" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">{{$static_data['strings']['next']}}</span>
                        </a>
                    @else
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <img class="responsive-img" src="{{ URL::asset('images/').'/no_image.jpg' }}"/>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="data">
                    <a href="{{url('/service').'/'.$service->alias}}"><h3 class="item-title primary-color">{{ $service->contentload->name }}</h3></a>
                    <div class="item-category">{{$service->location['address'].', '.$service->location['city'] .' - '. $service->location['country']}}</div>
                    <div class="item-category">{{$service->category->contentload->name.' - '.$service->ser_location->contentload->location}}</div>
                    @if($service->user)<div class="small-text">{{ $static_data['strings']['posted_by'] .': '. $service->user->username }}</div>@endif
                </div>
            </div>
        </div>
    @endforeach
@endif
@if($markers)
    <script type="text/javascript">
        var markers = [@foreach ($markers as $marker)[{{$marker['geo_lon']}}, {{$marker['geo_lat']}}], @endforeach],
                infoWindowContent = [@foreach ($services as $service)[{"id" : "{{$service->id}}","alias":"{{ $service->alias }}","name":{!! json_encode($service->contentload->name) !!},"address":"{{ $service->location['address'] }}" ,"city":"{{ $service->location['city'] }}" ,"country":"{{ $service->location['country'] }}" ,"phone":"{{ $service->contact['tel1'] }}", "icon":"{{ $service->category->map_icon }}", "featured":"{{ $service->featured }}", "image":@if(count($service->images))"{{ $service->images[0]->image }}" @else "no_image.jpg" @endif}], @endforeach];
        for(i = 0; i < markers.length; i++ ) {
            addMarkerToMap(markers[i][0], markers[i][1], infoWindowContent[i], 'service');
        }
    </script>
@endif