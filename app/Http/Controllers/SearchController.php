<?php

namespace App\Http\Controllers;

use App\Models\Admin\Property;
use App\Models\Admin\PropertyContent;
use App\Models\Admin\Service;
use App\Models\Admin\ServiceContent;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    protected $default_language, $static_data;
    public function __construct(){
        $this->default_language = default_language();
        $this->static_data = static_home();

    }

    public function index(Request $request){
        $default_language = $this->default_language;
        $static_data = $this->static_data;

        $term = $request->keyword ? $request->keyword : '';

        $property_ids = PropertyContent::where('name', 'LIKE', '%'.$term.'%')->get()->pluck('property_id');
        $service_ids = ServiceContent::where('name', 'LIKE', '%'.$term.'%')->get()->pluck('service_id');

        if($request->location_id && $request->category_id){

            $properties = Property::with(['images', 'contentload' => function($query) use($default_language){
                $query->where('language_id', $default_language->id);
            }])->where('status', 1)->whereIn('id', $property_ids)->where('location_id', $request->location_id)->where('category_id', $request->category_id)->get();

            $services = Service::with(['images', 'contentload' => function($query) use($default_language){
                $query->where('language_id', $default_language->id);
            }])->where('status', 1)->whereIn('id', $service_ids)->where('location_id', $request->location_id)->where('category_id', $request->category_id)->get();

        }else if($request->category_id){ // If there is only category

            $properties = Property::with(['images', 'contentload' => function($query) use($default_language){
                $query->where('language_id', $default_language->id);
            }])->where('status', 1)->whereIn('id', $property_ids)->where('category_id', $request->category_id)->get();

            $services = Service::with(['images', 'contentload' => function($query) use($default_language){
                $query->where('language_id', $default_language->id);
            }])->where('status', 1)->whereIn('id', $service_ids)->where('category_id', $request->category_id)->get();

        }else if($request->location_id){ // If there is only location

            $properties = Property::with(['images', 'contentload' => function($query) use($default_language){
                $query->where('language_id', $default_language->id);
            }])->where('status', 1)->whereIn('id', $property_ids)->where('location_id', $request->location_id)->get();

            $services = Service::with(['images', 'contentload' => function($query) use($default_language){
                $query->where('language_id', $default_language->id);
            }])->where('status', 1)->whereIn('id', $service_ids)->where('location_id', $request->location_id)->get();

        }else{ // If there are not category or location

            $properties = Property::with(['images', 'contentload' => function($query) use($default_language){
                $query->where('language_id', $default_language->id);
            }])->where('status', 1)->whereIn('id', $property_ids)->get();

            $services = Service::with(['images', 'contentload' => function($query) use($default_language){
                $query->where('language_id', $default_language->id);
            }])->where('status', 1)->whereIn('id', $service_ids)->get();
        }

        if(get_setting('allow_featured_properties','property')){
            $featured_properties = Property::with(['images', 'contentload' => function($query) use($default_language){
                $query->where('language_id', $default_language->id);
            }])->where('status', 1)->where('featured', 1)->inRandomOrder()->take(6)->get();
        }else{
            $featured_properties = null;
        }

        if(get_setting('allow_featured_services', 'service')){
            $featured_services = Service::with(['images', 'contentload' => function($query) use($default_language){
                $query->where('language_id', $default_language->id);
            }])->where('status', 1)->where('featured', 1)->inRandomOrder()->take(6)->get();
        }else{
            $featured_services = null;
        }

        return view('home.search', compact('services', 'static_data', 'default_language', 'featured_properties', 'featured_services',
            'services', 'properties'));
    }
}
